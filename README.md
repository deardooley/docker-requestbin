# [RequestBin](http://requestb.in)

Originally Created by [Jeff Lindsay](http://progrium.com)  
Docker contribution by [Rion Dooley](https://twitter.com/deardooley)

License
-------
MIT


## Requirements

Install [Docker Compose](https://docs.docker.com/compose/install/) if you do not have it already installed.

## Installation

Clone the project fro github

```
$ git clone https://bitbucket.org/agaveapi/requestbin  
```

From the project directory, run `docker-compose`:  

```
$ cd requestbin  
$ docker-compose up  
```  

This will run the automated build of the image and run it, exposing the app on port 5000.  

By default the app runs with a memory cache. If you would like to switch to a Redis backend, uncomment the commented out lines in `docker-compose.yml`. Once you have done this, run `docker-compose` again:  

```
$ docker-compose kill && docker-compose rm -f
$ docker-compose up  
```

This will pull down the trusted `redis` image and run with a mounted volume as a linked container to the RequestBin app. This should give you some fault tolerance between failures since Redis will not be ephimeral.  

The automated build is available in the Docker central repository as [agaveapi/requestbin](https://hub.docker.com/u/agaveapi/requestbin).  

## Run your own image manually  

Pull the image down from the Docker central repository:  

```
$ docker run -d -p "5000:5000" agaveapi/requestbin
```

This will start the container with the requestbin app available externally on port 5000.  To run the image with a Redis back end, you need to startup redis first. Preferably with a mounted volume.

```
$ docker run -d -v /usr/data:/data \
      --name some-redis  \
      redis redis-server --appendonly yes

$ docker run -d --link some-redis:redis  \
	  -e "STORAGE_BACKEND=requestbin.storage.redis.RedisStorage" \
	  -p "5000:5000" \
	  agaveapi/requestbin
```

Contributors
------------
 * Barry Carlyon <barry@barrycarlyon.co.uk>
 * Jeff Lindsay <progrium@gmail.com>
 * Rion Dooley <dooley@tacc.utexas.edu>
